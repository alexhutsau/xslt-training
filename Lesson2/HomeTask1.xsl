<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:h="HouseChema" 
                xmlns:i="HouseInfo" 
                xmlns:r="RoomsChema"
                exclude-result-prefixes="xs h i r"
                version="1.0">
    
    <xsl:output method="xml" indent="yes"/>
    <xsl:strip-space elements="*" />
    
    <xsl:template match="/">
        <AllRooms>
            <xsl:for-each select="h:Houses/h:House">                
                <xsl:sort select="@City"/>
                <xsl:sort select="./i:Address"/>
                
                <xsl:variable name="delimeter" select="'/'"/>
                <xsl:variable name="lower" select="'abcdefghijkmnopqrstuvyxwz'"/>
                <xsl:variable name="upper" select="'ABCDEFGHIJKMNOPQRSTUVYXWZ'"/>
                <xsl:variable name="address" select="concat(translate(@City, $lower, $upper), $delimeter, ./i:Address)"/>
                <xsl:variable name="houseRoomsCount" select="count(descendant::*[name() = 'r:Room'])"/>
                <xsl:variable name="houseGuestsCount" select="sum(descendant::*[name() = 'r:Room']/@guests)"/>
                <xsl:variable name="guestsPerRoomAverage" select="floor($houseGuestsCount div $houseRoomsCount)"/>
                
                <xsl:for-each select="./h:Blocks/h:Block">
                    <xsl:sort select="@number"/>
                    
                    <xsl:variable name="address2" select="concat($address, $delimeter, @number)"/>
                    <xsl:variable name="blockRoomsCount" select="count(descendant::*[name() = 'r:Room'])"/>
                    
                    <!--
                        Для второго дома в оригинальном файле отсутствует тэг <r:Rooms></r:Rooms>.
                        Я предпологаю, что это опечатка. 
                        И, соответственно, разрабатывал стили с учетом обязательного присутствия данного тэга. 
                     -->
                    <xsl:for-each select="./r:Rooms/r:Room">
                        <xsl:sort select="@nuber" data-type="number"/>
                        
                        <Room>
                           <Address>
                               <xsl:value-of select="concat($address2, $delimeter, @nuber)"/>
                           </Address>
                            <HouseRoomsCount>
                                <xsl:value-of select="$houseRoomsCount"/>
                            </HouseRoomsCount>
                            <BlockRoomsCount>
                                <xsl:value-of select="$blockRoomsCount"/>
                            </BlockRoomsCount>
                            <HouseGuestsCount>
                                <xsl:value-of select="$houseGuestsCount"/>
                            </HouseGuestsCount>
                            <GuestsPerRoomAverage>
                                <xsl:value-of select="$guestsPerRoomAverage"/>
                            </GuestsPerRoomAverage>
                            <Allocated Single="{@guests = 1}" Double="{@guests = 2}" Triple="{@guests = 3}" Quarter="{@guests = 4}"/>
                        </Room>
                    </xsl:for-each>
                </xsl:for-each>
            </xsl:for-each>
        </AllRooms>
    </xsl:template>

</xsl:stylesheet>